# movie-demo

Movie demo full-stack application

## Configuration

Fill in `.env.example` to `.env` and `.prod.env` files

```
MONGO_INITDB_ROOT_USERNAME=<db_username>
MONGO_INITDB_ROOT_PASSWORD=<db_password>
MONGO_CONNECTION_STRING=mongodb://<db_username>:<db_password>@<docker_ip>:<mongodb_port>/<db_name>
HOST=<docker_ip>
SESSION_SECRET=xxxxxx
```

## Development

This project is a mono-repo using [lerna](https://github.com/lerna/lerna).

If you do not have lerna installed, please install using the following command

```
yarn global add lerna
```

To install all dependencies for all inner packages

```
lerna bootstrap
```

To build and serve locally

```
lerna run build
yarn serve
```

Then open a browser and browse `http://localhost`

## Build Docker Image

For development

```
yarn build:dev
```

For production

```
yarn build:prod
```

## License

MIT © [Horace Lee](https://github.com/horacehylee)
