import * as express from "express";
const router: express.Router = express.Router();
import { check, validationResult } from "express-validator/check";

import {
  clearAllHistories,
  findAllHistories
} from "./../modules/movie-session-history";

export const HistoryRouter = () => {
  router.get("/", async (req: express.Request, res: express.Response) => {
    const histories = await findAllHistories(req.session)();
    res.json({
      histories
    });
  });

  router.delete("/", async (req: express.Request, res: express.Response) => {
    await clearAllHistories(req.session)();
    res.json({
      message: "histories are cleared"
    });
  });

  return router;
};
