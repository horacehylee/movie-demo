import * as express from "express";
const router: express.Router = express.Router();
import { check, validationResult } from "express-validator/check";

import { findAllMovies, findMovieById } from "./../data-loader/movies.loader";
import { addMovieHistory } from "./../modules/movie-session-history";

export const MoviesRouter = () => {
  router.get("/", async (req: express.Request, res: express.Response) => {
    const movies = await findAllMovies();
    res.json({
      movies
    });
  });

  router.post(
    "/:id/watch",
    async (req: express.Request, res: express.Response) => {
      const { id } = req.params;
      if (!id) {
        throw new Error("id should not be null");
      }
      const movie = await findMovieById(id);
      await addMovieHistory(req.session)(movie);
      res.json({
        message: `movie(${id}) is added to history`
      });
    }
  );

  return router;
};
