import { connect } from "mongoose";
import { setupMongoose } from "./util/mongoose";

const logError = console.error;
const log = console.log;

export const setupDb = async () => {
  setupMongoose();
  try {
    const connectionString = process.env.MONGO_CONNECTION_STRING;
    if (!connectionString) {
      throw new Error("connection string is missing");
    }
    await connect(connectionString, {
      config: {
        autoIndex: true
      },
      connectTimeoutMS: 5000
    } as any);
    log(`> connected to MongoDb at ${connectionString}`);
  } catch (e) {
    logError("> connecton error", e);
    throw e;
  }
};
