import * as mongoose from "mongoose";

export const setupMongoose = () => {
  (mongoose as any).Promise = global.Promise;
};
