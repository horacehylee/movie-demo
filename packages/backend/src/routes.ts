import * as express from "express";
import { join } from "path";

import { HistoryRouter } from "./routes/history.route";
import { MoviesRouter } from "./routes/movies.route";

export const setupRoutes = (app: express.Express) => {
  app.use("/", express.static(join(__dirname, "./../build/public")));

  app.use("/api/movies", MoviesRouter());
  app.use("/api/histories", HistoryRouter());
};
