export interface IVideoListResponse {
  totalCount: number;
  entries?: (IEntriesEntity)[] | null;
}
export interface IEntriesEntity {
  title: string;
  description: string;
  type: string;
  publishedDate: number;
  availableDate: number;
  metadata?: (IMetadataEntity)[] | null;
  contents?: (IContentsEntity)[] | null;
  credits?: (ICreditsEntity)[] | null;
  parentalRatings?: (IParentalRatingsEntity)[] | null;
  images?: (IImagesEntity)[] | null;
  categories?: (ICategoriesEntity)[] | null;
  id: string;
}
export interface IMetadataEntity {
  value: string;
  name: string;
}
export interface IContentsEntity {
  url: string;
  format: string;
  width: number;
  height: number;
  language: string;
  duration: number;
  geoLock: boolean;
  id: string;
}
export interface ICreditsEntity {
  role: string;
  name: string;
}
export interface IParentalRatingsEntity {
  scheme: string;
  rating: string;
}
export interface IImagesEntity {
  type: string;
  url: string;
  width: number;
  height: number;
  id: string;
}
export interface ICategoriesEntity {
  title: string;
  description: string;
  id: string;
}
