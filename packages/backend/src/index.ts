declare var __DEV__: boolean;

import * as errorHandler from "@horacehylee/api-error-handler";
import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as cors from "cors";
import * as express from "express";
import "express-async-errors";
import * as morgan from "morgan";

import { setupRoutes } from "./routes";
import { setupDb } from "./setup";

import { config } from "dotenv";
const configResult = config({
  path: "./../../.env"
});
if (configResult.error) {
  // throw configResult.error;
}

import * as connectMongo from "connect-mongo";
import * as session from "express-session";
import { connection } from "mongoose";
const MongoStore = connectMongo(session);

const port = +process.env.PORT || 80;

(async () => {
  await setupDb();

  const app: express.Express = express();

  app.use(bodyParser.json());
  app.use(compression());
  app.use(cors({ origin: true, credentials: true }));
  app.use(morgan("combined"));

  app.use(
    session({
      secret: process.env.SESSION_SECRET,
      store: new MongoStore({ mongooseConnection: connection }),
      resave: true,
      saveUninitialized: true,
      proxy: true,
      cookie: { secure: false }
    })
  );

  const log = console.log;
  const logError = console.error;

  app.listen(port, (err: Error) => {
    if (err) {
      logError(err);
      return;
    }
    if (__DEV__) {
      log("> in development");
    }
    log(`> listening on port ${port}`);
  });

  setupRoutes(app);

  app.use(
    errorHandler({
      production: !__DEV__
    })
  );
})();
