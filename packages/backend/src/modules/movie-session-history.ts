import { IEntriesEntity } from "../models/video-list-response.interface";

export const addMovieHistory = (session: Express.Session) => async (
  movie: IEntriesEntity
): Promise<void> => {
  let { histories } = session;
  if (!histories) {
    histories = [];
  }
  histories.push({
    ...movie,
    viewUtcTimestamp: Date.now()
  });
  session.histories = histories;
};

export const findAllHistories = (session: Express.Session) => async (): Promise<
  IEntriesEntity[]
> => {
  const { histories } = session;
  if (!histories) {
    return [];
  }
  return histories;
};

export const clearAllHistories = (
  session: Express.Session
) => async (): Promise<void> => {
  session.histories = [];
};
