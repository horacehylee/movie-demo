import {
  IEntriesEntity,
  IVideoListResponse
} from "./../models/video-list-response.interface";

import { find } from "lodash";

let videoListResponse: IVideoListResponse;
const loadResponse = async (): Promise<IVideoListResponse> => {
  if (!videoListResponse) {
    videoListResponse = require("./../data/movies.data.json");
  }
  return videoListResponse;
};

export const findAllMovies = async (): Promise<IEntriesEntity[]> => {
  const response = await loadResponse();
  return response.entries;
};

export const findMovieById = async (id: string): Promise<IEntriesEntity> => {
  const movies = await findAllMovies();
  const matchMovie = find(movies, movie => {
    return movie.id === id;
  });
  if (!matchMovie) {
    throw new Error(`movie(${id}) does not exist`);
  }
  return matchMovie;
};
