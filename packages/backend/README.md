# @movie-demo/backend

Backend module for movie-demo application

## Development

```
yarn serve
```

## Testing

```
yarn test
```

## License

MIT © [Horace Lee](https://github.com/horacehylee)
