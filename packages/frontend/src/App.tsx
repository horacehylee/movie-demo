import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./pages/Home";
import History from "./pages/History";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import { Provider } from "react-redux";
import { configureStore } from "./redux/store";
const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <Router>
            <div>
              <Route exact={true} path="/" component={Home} />
              <Route path="/history" component={History} />
            </div>
          </Router>
        </MuiThemeProvider>
      </Provider>
    );
  }
}
