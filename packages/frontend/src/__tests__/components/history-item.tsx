import React from "react";
import { shallow } from "enzyme";
import { HistoryItem } from "./../../components/history-item";
import { ListItem } from "material-ui";

describe("<HistoryItem />", () => {
  it(`should render with <ListItem />`, () => {
    const wrapper = shallow(
      <HistoryItem
        history={{
          title: "title",
          description: "description",
          type: "type",
          publishedDate: 123,
          availableDate: 123,
          id: "1",
          viewUtcTimestamp: 123,
          images: [
            { id: "1", height: 123, type: "type", width: 123, url: "hello" }
          ]
        }}
      />
    );
    expect(wrapper.type()).toBe(ListItem);
  });
});
