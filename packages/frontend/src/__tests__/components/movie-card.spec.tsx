import React from "react";
import { shallow } from "enzyme";
import { MovieCard } from "./../../components/movie-card";

describe("<MovieCard />", () => {
  it(`should render with "div" tag`, () => {
    const wrapper = shallow(<MovieCard title={"title"} />);
    expect(wrapper.type()).toBe("div");
  });

  it("should render title", () => {
    const wrapper = shallow(<MovieCard title={"My Title"} />);
    expect(wrapper.find(".title").length).toBe(1);
    expect(wrapper.find(".title").text()).toBe("My Title");
  });

  it("should use swiper-lazy class for image", () => {
    const wrapper = shallow(
      <MovieCard
        title={"My title"}
        coverImageUrl={"htp://placeholder.it/200/200"}
      />
    );
    expect(wrapper.find("img.swiper-lazy").length).toBe(1);
    expect(wrapper.find(`img.swiper-lazy[data-src="htp://placeholder.it/200/200"]`).length).toBe(1);
  });
});
