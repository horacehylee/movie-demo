import React from "react";
import { shallow } from "enzyme";
import { MovieVideo } from "./../../components/movie-video";
import { Player } from "video-react";

describe("<MovieVideo />", () => {
  it("should render <Player /> component", () => {
    const wrapper = shallow(
      <MovieVideo
        src={
          "http://d2bqeap5aduv6p.cloudfront.net/project_coderush_640x360_521kbs_56min.mp4"
        }
      />
    );
    expect(wrapper.find(Player).length).toEqual(1);
  });
});
