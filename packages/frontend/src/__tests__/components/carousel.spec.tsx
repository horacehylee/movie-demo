import React from "react";
import { shallow } from "enzyme";
import { Carousel } from "./../../components/carousel";
import { Swiper } from "react-dynamic-swiper";

describe("<Carousel />", () => {
  it(`should render with "div" tag`, () => {
    const wrapper = shallow(<Carousel />);
    expect(wrapper.type()).toBe("div");
  });

  it("should render <Swiper /> Component", () => {
    const wrapper = shallow(<Carousel />);
    expect(wrapper.find(Swiper).length).toBe(1);
  });
});
