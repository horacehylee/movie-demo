import React from "react";
import { mount } from "enzyme";
import { History } from "./../../pages/History";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { IconButton } from "material-ui";
import { HistoriesEntity } from "@movie-demo/apis";
import { HistoryItem } from "./../../components/history-item";

describe("<History />", () => {
  const fetchHistoriesMock = jest.fn();
  const historyMock = {
    goBack: jest.fn()
  };

  const historyComponent = (histories: HistoriesEntity[] = []) => (
    <MuiThemeProvider>
      <History
        fetchHistories={fetchHistoriesMock}
        histories={histories}
        history={historyMock}
      />
    </MuiThemeProvider>
  );

  it(`should call fetchHistory when didMount`, () => {
    const wrapper = mount(historyComponent());
    expect(fetchHistoriesMock.mock.calls.length).toBe(1);
  });

  it("should have IconButton component", () => {
    const wrapper = mount(historyComponent());
    expect(wrapper.find(IconButton).length).toBe(1);
    wrapper.find(IconButton).simulate("click");
    expect(historyMock.goBack.mock.calls.length).toBe(1);
  });

  it(`should render "Empty Watch History"`, () => {
    const wrapper = mount(historyComponent());
    expect(wrapper.find('div[children="Empty Watch History"]').length).toBe(1);
  });

  it("should render N <HistoryItem />", () => {
    const wrapper = mount(
      historyComponent([
        {
          title: "title",
          description: "description",
          type: "type",
          publishedDate: 123,
          availableDate: 123,
          id: "1",
          viewUtcTimestamp: 123,
          images: [
            { id: "1", height: 123, type: "type", width: 123, url: "hello" }
          ]
        }
      ])
    );
    expect(wrapper.find(HistoryItem).length).toBe(1);
  });
});
