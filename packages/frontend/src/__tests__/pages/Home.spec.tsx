import React from "react";
import { mount } from "enzyme";
import { Home } from "./../../pages/Home";
import { MuiThemeProvider } from "material-ui/styles";
import { MoviesEntity } from "@movie-demo/apis";
import { BrowserRouter as Router } from "react-router-dom";

describe("<Home />", () => {
  const fetchMoviesMock = jest.fn();
  const openDialogMock = jest.fn();
  const closeDialogMock = jest.fn();
  const selectMovieMock = jest.fn();
  const deselectMovieMock = jest.fn();
  const watchMovieMock = jest.fn();
  const carouselChangeMock = jest.fn();
  const homeComponent = (movies: MoviesEntity[] = []) => (
    <MuiThemeProvider>
      <Router>
        <Home
          movies={movies}
          modalIsOpen={false}
          selectedMovie={null}
          initalCarouselIndex={null}
          openDialog={openDialogMock}
          closeDialog={closeDialogMock}
          fetchMovies={fetchMoviesMock}
          selectMovie={selectMovieMock}
          deselectMovie={deselectMovieMock}
          watchMovie={watchMovieMock}
          carouselChange={carouselChangeMock}
        />
      </Router>
    </MuiThemeProvider>
  );

  it("should call fetchMovies when didMount", () => {
    const wrapper = mount(homeComponent());
    expect(fetchMoviesMock.mock.calls.length).toBe(1);
  });
});
