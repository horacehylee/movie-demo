import React, { Component, ReactNode, Props } from "react";
import "./carousel.scss";

import { Swiper, Slide } from "react-dynamic-swiper";
import "react-dynamic-swiper/lib/styles.css";

import renderIf from "render-if";

export interface ICarouselProps extends Props<Carousel> {
  width?: number;
  style?: any;
  slideIndex?: number;
  spaceBetween?: number;
  onKeyPress?: (activeIndex: number, keyCode: number) => void;
  onSlideChange?: (slideIndex: number) => void;
}

export interface ICarouselState {
  width?: number;
  height?: number;
}

export class Carousel extends Component<ICarouselProps, ICarouselState> {
  public static defaultProps: ICarouselProps = {
    slideIndex: 0,
    width: 250,
    spaceBetween: 0
  };

  swiper: any;

  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  componentWillReceiveProps(nextProps) {
    this.slideTo(nextProps.slideIndex);
  }

  // shouldComponentUpdate(nextProps) {
  //   return nextProps.children !== this.props.children;
  // }

  componentDidUpdate(prevProps: ICarouselProps) {
    if (prevProps.slideIndex === null || prevProps.slideIndex === undefined) {
      const childrenUpdated = prevProps.children !== this.props.children;
      const childrenCount = this.getChildrenCount();
      const halfChildrenCount = Math.floor(childrenCount / 2);
      const slidePerView = this.state.width / this.props.width;
      const halfSlidePreView = Math.floor(slidePerView / 2);
      const toSlide = Math.min(halfSlidePreView, halfChildrenCount);
      this.slideTo(toSlide);
      if (this.props.onSlideChange) {
        this.props.onSlideChange(toSlide);
      }
    }
  }

  slideTo = (index: number) => {
    if (this.swiper) {
      setTimeout(() => {
        this.swiper.slideTo(index, 0, false);
      });
    }
  };

  updateWindowDimensions = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  };

  getChildrenCount = () => {
    const { children } = this.props;
    return !children ? 0 : Array.isArray(children) ? children.length : 1;
  };

  getSwiperOption = () => {
    let { width, slideIndex, spaceBetween } = this.props;
    const slidePerView = this.state.width / width;
    return {
      initialSlide: slideIndex,
      slidesPerView: slidePerView,
      spaceBetween: spaceBetween,
      keyboardControl: true,
      mousewheelControl: false,
      centeredSlides: true,
      lazyLoading: true,
      lazyLoadingInPrevNext: false,
      preloadImages: false,
      onKeyPress: (swiper, keyCode) => {
        if (this.props && this.props.onKeyPress) {
          this.props.onKeyPress(swiper.activeIndex, keyCode);
        }
      },
      onSlideChangeEnd: swiper => {
        if (this.props && this.props.onSlideChange) {
          this.props.onSlideChange(swiper.activeIndex);
        }
      }
    };
  };

  renderSlides = () => {
    const { children } = this.props;
    if (Array.isArray(children)) {
      return children.map((item, i) => (
        <Slide className="carousel-side" key={i}>
          {item}
        </Slide>
      ));
    } else {
      return <Slide className="carousel-side">{children}</Slide>;
    }
  };

  onInitSwiper = swiper => {
    this.swiper = swiper;
  };

  render() {
    return (
      <div style={this.props.style}>
        <Swiper
          swiperOptions={this.getSwiperOption()}
          navigation={true}
          pagination={false}
          onInitSwiper={this.onInitSwiper}
        >
          {this.renderSlides()}
          {/* <Slide onActive={this.onAction}>Hello World</Slide> */} */}
        </Swiper>
      </div>
    );
  }
}
