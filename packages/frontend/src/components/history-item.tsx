import React, { Component } from "react";

import { ListItem } from "material-ui/List";
import { HistoriesEntity } from "@movie-demo/apis";

export interface IHistoryItemProps {
  history: HistoriesEntity;

  style?: any;
  onClick?: (history: HistoriesEntity) => (e: any) => void;
}

export class HistoryItem extends Component<IHistoryItemProps, {}> {
  render() {
    const { history, style } = this.props;
    const title = history.title;
    const coverImageUrl = history.images[0].url;
    return <ListItem primaryText={title} />;
  }
}
