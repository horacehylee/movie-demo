import React, { Component, Props } from "react";
import "./movie-card.scss";
import {
  Card,
  CardActions,
  CardHeader,
  CardText,
  CardMedia,
  CardTitle
} from "material-ui/Card";
// import Image from "react-lazy-image";

export interface IMovieCardProps extends Props<MovieCard> {
  title: string;
  coverImageUrl?: string;
  style?: any;
  onClick?: (event: any) => void;
}

export class MovieCard extends Component<IMovieCardProps, {}> {
  render() {
    const { title, coverImageUrl, style, onClick } = this.props;
    return (
      <div className="card" style={style} onClick={onClick}>
        <div className="coverImage">
          <img className="swiper-lazy" data-src={coverImageUrl} />
          <div className="swiper-lazy-preloader" />
        </div>
        <span className="title">{title}</span>
      </div>
    );
  }
}
