import React, { Component } from "react";

// import "node_modules/video-react/dist/video-react.css";
import "./movie-video.scss";
import { Player } from "video-react";

export interface IMovieVideoProps {
  src: string;
  autoPlay?: boolean;
  onFinish?: () => void;
}

export class MovieVideo extends Component<IMovieVideoProps, {}> {
  player: any;

  onEnded = e => {
    if (this.props && this.props.onFinish) {
      this.props.onFinish();
    }
  };

  render() {
    const { src, autoPlay } = this.props;

    return (
      <Player
        onEnded={this.onEnded}
        autoPlay={this.props.autoPlay}
        ref={ref => (this.player = ref)}
      >
        <source src={src} />
      </Player>
    );
  }
}
