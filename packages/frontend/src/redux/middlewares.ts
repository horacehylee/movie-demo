import logger from "redux-logger";

import { rootEpic } from "./epics";
import { createEpicMiddleware } from "redux-observable";

const epicMiddleware = createEpicMiddleware(rootEpic);

const middlewares = [epicMiddleware];

declare const __DEV__: boolean;
if (__DEV__) {
  middlewares.push(logger);
}

export { middlewares };
