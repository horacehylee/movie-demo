import { combineReducers } from "redux";
import { moviesReducer } from "./movies/movies.reducer";
import { historiesReducer } from "./histories/histories.reducer";
import { homePageReducer } from "./homePage/homePage.reducer";
import { historyPageReducer } from "./historyPage/historyPage.reducer";

export const rootReducer = combineReducers({
  data: combineReducers({
    movies: moviesReducer,
    histories: historiesReducer
  }),
  appState: combineReducers({
    homePage: homePageReducer,
    historyPage: historyPageReducer,
  })
});
