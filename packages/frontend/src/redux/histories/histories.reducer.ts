import { getType } from "typesafe-actions";
import { historiesActions } from "./histories.actions";
import { updateObject } from "./../util";

export const historiesReducer = (state = [], action) => {
  switch (action.type) {
    case getType(historiesActions.update):
      return [...action.payload];
    default:
      return state;
  }
};
