import { createAction } from "typesafe-actions";
import { HistoriesEntity } from "@movie-demo/apis";

export const historiesActions = {
  update: createAction("HISTORIES_UPDATE", (histories: HistoriesEntity[]) => ({
    type: "HISTORIES_UPDATE",
    payload: histories
  }))
};
