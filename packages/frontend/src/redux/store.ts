import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "./reducers";
import { middlewares } from "./middlewares";

export const configureStore = (initialState?: any) => {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  );
};
