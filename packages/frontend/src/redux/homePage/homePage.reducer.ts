import { getType } from "typesafe-actions";
import { homePageActions } from "./homePage.actions";
import { updateObject } from "./../util";

const initalState = {
  modalIsOpen: false,
  selectedMovie: null,
  loading: false,
  error: null,
  carouselIndex: null
};

export const homePageReducer = (state = initalState, action) => {
  switch (action.type) {
    case getType(homePageActions.openModal):
      return updateObject(state, {
        ...state,
        modalIsOpen: true
      });

    case getType(homePageActions.closeModal):
      return updateObject(state, {
        ...state,
        modalIsOpen: false
      });

    case getType(homePageActions.selectMovie):
      return updateObject(state, {
        ...state,
        selectedMovie: action.payload
      });
    case getType(homePageActions.deselectMovie):
      return updateObject(state, {
        ...state,
        selectedMovie: null
      });
    case getType(homePageActions.fetchMovies):
      return updateObject(state, {
        ...state,
        loading: true
      });
    case getType(homePageActions.fetchMoviesFulfilled):
      return updateObject(state, {
        ...state,
        loading: false
      });
    case getType(homePageActions.fetchMoviesError):
      return updateObject(state, {
        ...state,
        loading: false,
        error: action.payload
      });
    case getType(homePageActions.carouselChange):
      return updateObject(state, {
        ...state,
        carouselIndex: action.payload
      });
    default:
      return state;
  }
};
