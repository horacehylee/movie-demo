import { createAction } from "typesafe-actions";
import { MoviesEntity } from "@movie-demo/apis";

const fetchMoviesActions = () => ({
  fetchMovies: createAction("HOMEPAGE_FETCH_MOVIES"),
  fetchMoviesFulfilled: createAction(
    "HOMEPAGE_FETCH_MOVIES_FULFILLED",
    (movies: MoviesEntity[]) => ({
      type: "HOMEPAGE_FETCH_MOVIES_FULFILLED",
      payload: movies
    })
  ),
  fetchMoviesError: createAction("HOMEPAGE_FETCH_MOVIES_ERROR", error => ({
    type: "HOMEPAGE_FETCH_MOVIES_ERROR",
    payload: error
  }))
});

const watchMovieActions = () => ({
  watchMovie: createAction("HOMEPAGE_WATCH_MOVIE", (movieId: string) => ({
    type: "HOMEPAGE_WATCH_MOVIE",
    payload: {
      movieId
    }
  })),
  watchMovieFulfilled: createAction("HOMEPAGE_WATCH_MOVIE_FULFILLED"),
  watchMovieError: createAction("HOMEPAGE_WATCH_MOVIE_ERROR", error => ({
    type: "HOMEPAGE_WATCH_MOVIE_ERROR",
    payload: error
  }))
});

export const homePageActions = {
  selectMovie: createAction(
    "HOMEPAGE_SELECT_MOVIE",
    (selectedMovie: MoviesEntity) => ({
      type: "HOMEPAGE_SELECT_MOVIE",
      payload: selectedMovie
    })
  ),
  deselectMovie: createAction("HOMEPAGE_DESELECT_MOVIE"),
  openModal: createAction("HOMEPAGE_OPEN_MODAL"),
  closeModal: createAction("HOMEPAGE_CLOSE_MODAL"),
  carouselChange: createAction("HOMEPAGE_CAROUSEL_CHANGE", (index: number) => ({
    type: "HOMEPAGE_CAROUSEL_CHANGE",
    payload: index
  })),
  ...fetchMoviesActions(),
  ...watchMovieActions()
};
