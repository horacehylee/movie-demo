import { createAction } from "typesafe-actions";
import { HistoriesEntity } from "@movie-demo/apis";

const fetchHistoriesActions = () => ({
  fetchHistories: createAction("HISTORYPAGE_FETCH_HISTORIES"),
  fetchHistoriesFulfilled: createAction(
    "HISTORYPAGE_FETCH_HISTORIES_FULFILLED",
    (histories: HistoriesEntity[]) => ({
      type: "HISTORYPAGE_FETCH_HISTORIES_FULFILLED",
      payload: histories
    })
  ),
  fetchHistoriesError: createAction(
    "HISTORYPAGE_FETCH_HISTORIES_ERROR",
    error => ({
      type: "HISTORYPAGE_FETCH_HISTORIES_ERROR",
      payload: error
    })
  )
});

export const historyPageActions = {
  ...fetchHistoriesActions()
};
