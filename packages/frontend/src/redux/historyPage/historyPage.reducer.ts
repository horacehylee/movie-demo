import { getType } from "typesafe-actions";
import { historyPageActions } from "./historyPage.actions";
import { updateObject } from "./../util";

const initalState = {
  loading: false,
  error: null
};

export const historyPageReducer = (state = initalState, action) => {
  switch (action.type) {
    case getType(historyPageActions.fetchHistories):
      return updateObject(state, {
        ...state,
        loading: true
      });

    case getType(historyPageActions.fetchHistoriesFulfilled):
      return updateObject(state, {
        ...state,
        loading: false
      });

    case getType(historyPageActions.fetchHistoriesError):
      return updateObject(state, {
        ...state,
        loading: false,
        error: action.payload
      });
    default:
      return state;
  }
};
