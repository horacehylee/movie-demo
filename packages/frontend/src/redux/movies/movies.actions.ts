import { createAction } from "typesafe-actions";
import { MoviesEntity } from "@movie-demo/apis";

export const moviesActions = {
  update: createAction("MOVIES_UPDATE", (movies: MoviesEntity[]) => ({
    type: "MOVIES_UPDATE",
    payload: movies
  }))
};
