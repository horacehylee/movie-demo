import { getType } from "typesafe-actions";
import { moviesActions } from "./movies.actions";
import { updateObject } from "./../util";

export const moviesReducer = (state = [], action) => {
  switch (action.type) {
    case getType(moviesActions.update):
      return [...action.payload];
    default:
      return state;
  }
};
