import { combineEpics } from "redux-observable";

import { isActionOf } from "typesafe-actions";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/observable/concat";

import { homePageActions } from "./homePage/homePage.actions";
import { moviesActions } from "./movies/movies.actions";

import {
  apis,
  MovieListSuccessResponse,
  WatchMovieSuccessResponse
} from "@movie-demo/apis";
import { historyPageActions } from "./historyPage/historyPage.actions";
import { historiesActions } from "./histories/histories.actions";

const fetchMoviesEpic = (action$, store) =>
  action$.filter(isActionOf(homePageActions.fetchMovies)).mergeMap(action =>
    Observable.fromPromise(apis.movies.list())
      .flatMap(response => {
        const { movies } = response;
        return Observable.concat(
          Observable.of(moviesActions.update(movies)),
          Observable.of(homePageActions.fetchMoviesFulfilled(movies))
        );
      })
      .catch(err => Observable.of(homePageActions.fetchMoviesError(err)))
  );

const watchMovieEpic = (action$, store) =>
  action$.filter(isActionOf(homePageActions.watchMovie)).mergeMap(action =>
    Observable.fromPromise(
      apis.movies.watch({
        id: action.payload.movieId
      })
    )
      .flatMap(response => {
        return Observable.of(homePageActions.watchMovieFulfilled());
      })
      .catch(err => Observable.of(homePageActions.watchMovieError(err)))
  );

const fetchHistoriesEpic = (action$, store) =>
  action$
    .filter(isActionOf(historyPageActions.fetchHistories))
    .mergeMap(action =>
      Observable.fromPromise(apis.histories.list())
        .flatMap(response => {
          const { histories } = response;
          return Observable.concat(
            Observable.of(historiesActions.update(histories)),
            Observable.of(historyPageActions.fetchHistoriesFulfilled(histories))
          );
        })
        .catch(err =>
          Observable.of(historyPageActions.fetchHistoriesError(err))
        )
    );

export const rootEpic = combineEpics(
  fetchMoviesEpic,
  watchMovieEpic,
  fetchHistoriesEpic
);
