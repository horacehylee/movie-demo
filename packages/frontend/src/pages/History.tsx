import React, { Component } from "react";

import Flexbox from "@horacehylee/flexbox-react";
import { AppBar, FlatButton, IconButton, List, Paper } from "material-ui";
import { NavigationArrowBack } from "material-ui/svg-icons";
import { HistoryItem } from "./../components/history-item";
import { isEmpty, sortBy, reverse } from "lodash";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { historyPageActions } from "../redux/historyPage/historyPage.actions";
import { HistoriesEntity } from "@movie-demo/apis";

import { RouterProps } from "react-router";

const sortHistoriesByViewTime = (
  histories: HistoriesEntity[]
): HistoriesEntity[] => {
  return reverse(
    sortBy(histories, (history: HistoriesEntity) => {
      return history.viewUtcTimestamp;
    })
  );
};

export interface IHistoryProps extends RouterProps {
  histories: HistoriesEntity[];

  fetchHistories: () => any;
}

export class History extends Component<IHistoryProps, {}> {
  componentDidMount() {
    this.props.fetchHistories();
  }

  goBack = () => {
    const { history } = this.props;
    history.goBack();
  };

  renderHistoryItem = () => {
    const { histories } = this.props;
    if (isEmpty(histories)) {
      return <div>Empty Watch History</div>;
    } else {
      const sortedHistories = sortHistoriesByViewTime(histories);
      const historyItems = sortedHistories.map((history, i) => {
        return <HistoryItem history={history} key={i} />;
      });
      return <List>{historyItems}</List>;
    }
  };

  render() {
    return (
      <Flexbox flexGrow={1} flexDirection={"column"}>
        <AppBar
          title={"History"}
          showMenuIconButton={true}
          iconElementLeft={
            <IconButton onClick={this.goBack}>
              <NavigationArrowBack />
            </IconButton>
          }
        />
        <Flexbox flexGrow={1} flexDirection={"column"}>
          <Paper>{this.renderHistoryItem()}</Paper>
        </Flexbox>
      </Flexbox>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let historyPageState = state.appState.historyPage;
  let historiesData = state.data.histories;
  return {
    histories: historiesData,
    loading: historyPageState.loading,
    error: historyPageState.error
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchHistories: historyPageActions.fetchHistories
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(History);
