import React, { Component } from "react";

import Flexbox from "@horacehylee/flexbox-react";
import { AppBar, FlatButton, Dialog } from "material-ui";
import { Carousel } from "./../components/carousel";
import { MovieCard } from "./../components/movie-card";

import { MovieVideo } from "./../components/movie-video";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { homePageActions } from "./../redux/homePage/homePage.actions";
import { MoviesEntity } from "@movie-demo/apis";

interface IHomeProps {
  movies: MoviesEntity[];
  modalIsOpen: boolean;
  selectedMovie: MoviesEntity;
  initalCarouselIndex: number;

  openDialog: () => any;
  closeDialog: () => any;
  fetchMovies: () => any;
  selectMovie: (selectedMovie: MoviesEntity) => any;
  deselectMovie: () => any;
  watchMovie: (movieId: string) => any;
  carouselChange: (index: number) => any;
}

export class Home extends Component<IHomeProps, {}> {
  _currentCarouselIndex: number = 0;

  componentDidMount() {
    this.props.fetchMovies();
  }

  componentWillUnmount() {
    this.props.carouselChange(this._currentCarouselIndex);
  }

  movieCardClick = (index: number, movie: MoviesEntity) => () => {
    this.props.selectMovie(movie);
    this.props.watchMovie(movie.id);
    this.props.carouselChange(index);
    this.props.openDialog();
  };

  closeDialog = () => {
    this.props.deselectMovie();
    this.props.closeDialog();
  };

  onKeyPressCarousel = (activeIndex, keyCode) => {
    if (keyCode === 13) {
      const { movies } = this.props;
      this.movieCardClick(activeIndex, movies[activeIndex])();
    }
  };

  onCarouselIndexChange = (slideIndex: number) => {
    this._currentCarouselIndex = slideIndex;
  };

  renderMovieCards = () => {
    if (this.props.movies) {
      let { movies } = this.props;
      return movies.map((video, i) => {
        const { title, images } = video;
        const coverImageUrl = images[0].url;
        return (
          <MovieCard
            title={title}
            coverImageUrl={coverImageUrl}
            key={i}
            onClick={this.movieCardClick(i, video)}
            style={{
              width: "230px"
            }}
          />
        );
      });
    }
    return null;
  };

  renderMovieDialog = () => {
    const { selectedMovie } = this.props;
    if (selectedMovie) {
      const movieTitle = selectedMovie.title;
      const movieSrc = selectedMovie.contents[0].url;

      return (
        <Dialog
          title={movieTitle}
          modal={false}
          open={this.props.modalIsOpen}
          onRequestClose={this.closeDialog}
        >
          <div style={{ height: "400px" }}>
            <MovieVideo
              src={movieSrc}
              autoPlay={true}
              onFinish={this.closeDialog}
            />
          </div>
        </Dialog>
      );
    }
    return null;
  };

  render() {
    return (
      <Flexbox flexGrow={1} flexDirection={"column"}>
        <AppBar
          title={"Home"}
          showMenuIconButton={false}
          iconElementRight={
            <FlatButton
              label="History"
              containerElement={<Link to={"/history"} />}
            />
          }
        />
        <Carousel
          width={250}
          style={{
            marginTop: "20px"
          }}
          onKeyPress={this.onKeyPressCarousel}
          slideIndex={this.props.initalCarouselIndex}
          onSlideChange={this.onCarouselIndexChange}
        >
          {this.renderMovieCards()}
        </Carousel>

        {this.renderMovieDialog()}
      </Flexbox>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let homePageState = state.appState.homePage;
  let moviesData = state.data.movies;
  return {
    movies: moviesData,
    modalIsOpen: homePageState.modalIsOpen,
    selectedMovie: homePageState.selectedMovie,
    loading: homePageState.loading,
    error: homePageState.error,
    initalCarouselIndex: homePageState.carouselIndex
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      openDialog: homePageActions.openModal,
      closeDialog: homePageActions.closeModal,
      fetchMovies: homePageActions.fetchMovies,
      selectMovie: homePageActions.selectMovie,
      deselectMovie: homePageActions.deselectMovie,
      watchMovie: homePageActions.watchMovie,
      carouselChange: homePageActions.carouselChange
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
