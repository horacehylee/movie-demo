const webpack = require("webpack");
const rewireCssModules = require("@horacehylee/react-app-rewire-css-modules");
const path = require("path");

module.exports = function override(config, env) {
  config = rewireCssModules(config, env);

  config.resolve.alias["swiper"] = path.resolve(
    __dirname,
    "node_modules/@horacehylee/swiper"
  );

  config.plugins.push(
    new webpack.DefinePlugin({
      __DEV__: env === "development"
    })
  );
  return config;
};
