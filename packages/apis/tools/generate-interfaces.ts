import { exec, echo, cd, ls } from "shelljs";
import { basename, join } from "path";
import chalk from "chalk";
import { isEmpty } from "lodash";
import { pascalCase } from "change-case";

const pretty = obj => JSON.stringify(obj, null, 2);
const makeTypeBinPath = join("node_modules", ".bin", "make_types");

(() => {
  echo("Generating interfaces...");

  const apiSampleJsonFileName = ls("samples");
  if (!apiSampleJsonFileName || isEmpty(apiSampleJsonFileName)) {
    echo("No samples are found.");
    return;
  }
  echo(
    `Found ${apiSampleJsonFileName.length} JSON files from "samples" folder`
  );
  apiSampleJsonFileName.forEach(fileName => {
    echo(`  - ${chalk.blue(fileName)}`);
  });

  apiSampleJsonFileName.forEach(fileName => {
    const baseFileName = basename(fileName, ".json");
    const rootName = `${pascalCase(baseFileName)}Response`;
    exec(
      `${makeTypeBinPath} -i src/interfaces/${baseFileName}.ts samples/${fileName} ${rootName}`
    );
  });
  echo("Interfaces are generated!!");
})();
