# @movie-demo/apis

API module for movie-demo application

## Development

```
yarn serve
```

## Build

```
yarn build
```

## Test

```
yarn test
```

## License

MIT © [Horace Lee](https://github.com/horacehylee)
