import { apis, MoviesEntity } from "./../index";

const expectMovieIsDefined = (movie: MoviesEntity) => {
  expect(movie).not.toBeUndefined();
  expect(movie.id).not.toBeUndefined();
  expect(movie.title).not.toBeUndefined();
  expect(movie.images[0].url).not.toBeUndefined();
};

describe("apis object", () => {
  it("should has all necessary methods", () => {
    expect(apis.movies).not.toBeUndefined();
    expect(apis.movies.list).not.toBeUndefined();
    expect(apis.movies.watch).not.toBeUndefined();
    expect(apis.histories).not.toBeUndefined();
  });
});

describe("movies list api", () => {
  it("should response with a list of movies", async () => {
    const movieResponse = await apis.movies.list();
    expect(movieResponse).not.toBeUndefined();
    expect(movieResponse.movies).not.toBeUndefined();
    expectMovieIsDefined(movieResponse.movies[0]);
  });
});

describe("movies watch api", () => {
  let movieList: MoviesEntity[] = [];

  beforeAll(async () => {
    const { movies } = await apis.movies.list();
    movieList = movies;
    expectMovieIsDefined(movieList[0]);
  });

  it("should pass watching valid id", async () => {
    expectMovieIsDefined(movieList[0]);
    const watchResponse = await apis.movies.watch({
      id: movieList[0].id
    });
    expect(watchResponse).not.toBeUndefined();
    expect(watchResponse.message).not.toBeUndefined();
  });

  it("should fail for watching unknown id", async () => {
    const unknownId = "MFvszr:e7kCx/wq=";
    await expect(
      apis.movies.watch({
        id: unknownId
      })
    ).rejects.toThrowError("status code 500");
  });

  it("should fail for passing null", async () => {
    await expect(apis.movies.watch(undefined)).rejects.toThrowError("id");
    await expect(apis.movies.watch(null)).rejects.toThrowError("id");
    await expect(
      apis.movies.watch({
        id: undefined
      })
    ).rejects.toThrowError("id");
    await expect(
      apis.movies.watch({
        id: null
      })
    ).rejects.toThrowError("id");
  });
});

describe("histories list api", () => {
  it("should response with a list of histories", async () => {
    const historyResponse = await apis.histories.list();
    expect(historyResponse).not.toBeUndefined();
    expect(historyResponse.histories).not.toBeUndefined();
  });

  it("should response as history after watching movie", async () => {
    const { movies } = await apis.movies.list();
    await apis.movies.watch({
      id: movies[0].id
    });
    const historyResponse = await apis.histories.list();
  });
});

describe("histories clear api", () => {
  it("should success calling clear", async () => {
    await apis.histories.clear();
  });
});
