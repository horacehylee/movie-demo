export interface MovieListSuccessResponse {
  movies?: (MoviesEntity)[] | null;
}
export interface MoviesEntity {
  title: string;
  description: string;
  type: string;
  publishedDate: number;
  availableDate: number;
  metadata?: (MetadataEntity)[] | null;
  contents?: (ContentsEntity)[] | null;
  credits?: (CreditsEntity)[] | null;
  parentalRatings?: (ParentalRatingsEntity)[] | null;
  images?: (ImagesEntity)[] | null;
  categories?: (CategoriesEntity)[] | null;
  id: string;
}
export interface MetadataEntity {
  value: string;
  name: string;
}
export interface ContentsEntity {
  url: string;
  format: string;
  width: number;
  height: number;
  language: string;
  duration: number;
  geoLock: boolean;
  id: string;
}
export interface CreditsEntity {
  role: string;
  name: string;
}
export interface ParentalRatingsEntity {
  scheme: string;
  rating: string;
}
export interface ImagesEntity {
  type: string;
  url: string;
  width: number;
  height: number;
  id: string;
}
export interface CategoriesEntity {
  title: string;
  description: string;
  id: string;
}
