import {
  ContentsEntity,
  MetadataEntity,
  CreditsEntity,
  ParentalRatingsEntity,
  ImagesEntity,
  CategoriesEntity
} from "./movie-list-success";

export interface HistoryListSuccessResponse {
  histories?: (HistoriesEntity)[] | null;
}
export interface HistoriesEntity {
  title: string;
  description: string;
  type: string;
  publishedDate: number;
  availableDate: number;
  metadata?: (MetadataEntity)[] | null;
  contents?: (ContentsEntity)[] | null;
  credits?: (CreditsEntity)[] | null;
  parentalRatings?: (ParentalRatingsEntity)[] | null;
  images?: (ImagesEntity)[] | null;
  categories?: (CategoriesEntity)[] | null;
  id: string;
  viewUtcTimestamp: number;
}
