import axios from "axios";
import { MovieListSuccessResponse } from "./interfaces/movie-list-success";
import { WatchMovieSuccessResponse } from "./interfaces/watch-movie-success";
import { HistoryListSuccessResponse } from "./interfaces/history-list-success";
import { HistoryClearSuccessResponse } from "./interfaces/history-clear-success";

declare const __HOST__: string;

const defaultUrl = __HOST__ || "http://localhost";

export interface IMovieDemoOptions {
  rootUrl?: string;
}

export interface IMoviesWatchParams {
  id: string;
}

export const MovieDemo = (options?: IMovieDemoOptions) => {
  options = options || {};
  const baseUrl = options.rootUrl || defaultUrl;

  const axiosInstance = axios.create({
    baseURL: baseUrl,
    credentials: "include",
    timeout: 3000,
    withCredentials: true
  } as any);

  return {
    histories: {
      async list(): Promise<HistoryListSuccessResponse> {
        const response = await axiosInstance.get(`/api/histories`);
        return response.data;
      },
      async clear(): Promise<HistoryClearSuccessResponse> {
        const response = await axiosInstance.delete("/api/histories");
        return response.data;
      }
    },
    movies: {
      async list(): Promise<MovieListSuccessResponse> {
        const response = await axiosInstance.get(`/api/movies`);
        return response.data;
      },
      async watch(
        params: IMoviesWatchParams
      ): Promise<WatchMovieSuccessResponse> {
        const { id } = params;

        if (!id) {
          throw new Error("id cannot be null or undefined");
        }
        const encodedId = encodeURIComponent(id);
        const response = await axiosInstance.post(
          `/api/movies/${encodedId}/watch`
        );
        return response.data;
      }
    }
  };
};
