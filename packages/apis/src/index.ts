export * from "./interfaces/history-list-success";
export * from "./interfaces/movie-list-success";
export * from "./interfaces/watch-movie-success";
export * from "./interfaces/history-clear-success";

export { IMovieDemoOptions, IMoviesWatchParams } from "./movie-demo";

import { HistoryClearSuccessResponse } from "./interfaces/history-clear-success";
import { HistoryListSuccessResponse } from "./interfaces/history-list-success";
import { MovieListSuccessResponse } from "./interfaces/movie-list-success";
import { WatchMovieSuccessResponse } from "./interfaces/watch-movie-success";

import { IMoviesWatchParams, MovieDemo } from "./movie-demo";
const apis = MovieDemo();
export { apis, MovieDemo };
