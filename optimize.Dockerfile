# ---- Base Node ----
FROM mhart/alpine-node:9 AS base

# ---- Dependencies ----
FROM base AS dependencies
RUN yarn global add lerna
# Setup for bootstrap for caching
WORKDIR /tmp
COPY package.json lerna.json /tmp/
COPY packages/apis/package.json packages/apis/yarn.lock /tmp/packages/apis/
COPY packages/frontend/package.json packages/frontend/yarn.lock /tmp/packages/frontend/
COPY packages/backend/package.json packages/backend/yarn.lock /tmp/packages/backend/
# Production packages
RUN lerna clean --yes
RUN lerna bootstrap --production
RUN cp -a /tmp/ /tmp-prod/
# Development packages
RUN lerna bootstrap

# ---- Builder ----
FROM dependencies as builder
ARG HOST
WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN cp -a /tmp/packages /usr/src/app/
# > apis
WORKDIR /usr/src/app/packages/apis
RUN HOST=$HOST yarn build
# > frontend
WORKDIR /usr/src/app/packages/frontend
RUN CI=true yarn test
RUN yarn build
# > backend
WORKDIR /usr/src/app/packages/backend
RUN yarn test
RUN yarn build

# ---- Release ----
FROM base AS release
WORKDIR /usr/src/app
# Copy source code
COPY . /usr/src/app/
# Copy production dependencies
COPY --from=dependencies /tmp-prod/packages/backend /usr/src/app/packages/backend/
# Copy build artifacts
COPY --from=builder /usr/src/app/packages/backend/build /usr/src/app/packages/backend/build/
# Run application
WORKDIR /usr/src/app/packages/backend
ENV NODE_ENV=production
ENV PORT=80
CMD [ "yarn", "start" ]
EXPOSE 80